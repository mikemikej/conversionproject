﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConversionProject
{
    public class Converter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _hexadecimalString;
        public string HexadecimalString
        {
            get { return _hexadecimalString; }
            set
            {
                _hexadecimalString = value;
                RaisePropertyChanged("HexadecimalString");
            }
        }


        private string _binaryStringNib;
        
        public string BinaryStringNib
        {
            get { return _binaryStringNib; }
            set
            {
                _binaryStringNib = value;
                RaisePropertyChanged("BinaryStringNib");
            }
        }

        private string _binaryString;
        public string BinaryString
        {
            get { return _binaryString; }
            set
            {
                _binaryString = value;
                RaisePropertyChanged("BinaryString");
            }
        }


        private string _decimalString;

        public string DecimalString        
        {
            get { return _decimalString; }
            set
            {
                _decimalString = value;
                RaisePropertyChanged("DecimalString");
            }
        }


        private string ConvertIntegerToHex(string intString)
        {
            string returnAsHex = int.Parse(intString).ToString("X");
            return returnAsHex;
        }

        private string ConvertIntegerToBinary(string intString)
        {
            string returnAsBinary = Convert.ToString(int.Parse(intString), 2).PadLeft(8,'0');
            return returnAsBinary;
        }

        private int ConvertHextoInteger(string hexString)
        {
            int toReturn = int.Parse(hexString, System.Globalization.NumberStyles.HexNumber);
            return toReturn;
        }

        private string ConvertHextoBinary(string hexString)
        {
            int HexAsInt = ConvertHextoInteger(hexString);
            string toReturn = ConvertIntegerToBinary(HexAsInt.ToString());

            return toReturn;
        }

        private int ConvertBinarytoInteger(string binaryString)
        {
            int binaryInt = Convert.ToInt32(binaryString, 2);

            return binaryInt;
        }

        private string ConvertBinaryToHex(string binaryString)
        {
            int binaryInt = ConvertBinarytoInteger(binaryString);
            string toReturn = ConvertIntegerToHex(binaryInt.ToString());
            return toReturn;
        }

        public void ConvertInt()
        {
            int isNumber = 0;

            if (int.TryParse(_decimalString, out isNumber))
            {
                HexadecimalString = ConvertIntegerToHex(_decimalString);
                BinaryString = ConvertIntegerToBinary(_decimalString);
                BinaryStringNib = Regex.Replace(BinaryString, ".{4}", "$0 ");
            }
            else
            {
                BinaryStringNib = "Invalid Decimal Input!";
            }
        }

        static readonly Regex binary = new Regex("^[01]{1,32}$", RegexOptions.Compiled);
        public void ConvertBinary()
        {
            if (binary.IsMatch(_binaryString))
            {
                

                HexadecimalString = ConvertBinaryToHex(_binaryString);
                DecimalString = ConvertBinarytoInteger(_binaryString).ToString();
                BinaryStringNib = Regex.Replace(Convert.ToString(int.Parse(_binaryString), 2).PadLeft(8, '0'), ".{4}", "$0 ");
                BinaryString = Regex.Replace(_binaryString, ".{4}", "$0 ").PadLeft(8, '0');
            }
            else
            {
                BinaryStringNib = "Invalid Binary Input!";
            }
        }

        public void ConvertHex()
        {
            int isNumber = 0;

            if (int.TryParse(_hexadecimalString,
                     System.Globalization.NumberStyles.HexNumber,
                     System.Globalization.CultureInfo.InvariantCulture, out isNumber))
            {
                BinaryString = ConvertHextoBinary(_hexadecimalString);
                DecimalString = ConvertHextoInteger(_hexadecimalString).ToString();
                BinaryStringNib = Regex.Replace(BinaryString, ".{4}", "$0 ");
            }
            else
            {
                BinaryStringNib = "Invalid Hex Input!";
            }
        }

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ConvertEmptyVals(object obj)
        {
            if (_hexadecimalString != null)
            {
                ConvertHex();
            }
            else if(_decimalString != null)
            {
                ConvertInt();
            }
            else if (_binaryString != null)
            {
                ConvertBinary();
            }
        }

        public void ResetVals(object obj)
        {
            BinaryString = null;
            DecimalString = null;
            HexadecimalString = null;
            BinaryStringNib = null;
        }

    }
}
