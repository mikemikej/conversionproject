﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionProject.ViewModels
{
    class ConverterVM 
    {
        public Converter ConverterObj { get; set; }

        public Command ConvertVals { get; set; }

        public Command ResetVals { get; set; }

        public ConverterVM()
        {
            ConverterObj = new Converter();
            ConvertVals = new Command();
            ResetVals = new Command();
            ConvertVals.ExecuteCommand = new Action<object>(ConverterObj.ConvertEmptyVals);
            ResetVals.ExecuteCommand = new Action<object>(ConverterObj.ResetVals);
        }
        


    }
}
